


---

## Lab 5A

### Instructions

Using your calculator you created from Lab4A, split up the functionality into modules and utilize packaging. Some things you could split up:

* The user menu into it's own module on a higher level package
* Operations into one module, lower level
* Algorithms into one module, lower level
* etc

### Requirements

* All requirements from Lab4A
* Utilize clean and proper dir and module names

---



